# Software Studio 2021 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets(blur)                            | 1~5%      | Y         |


---

### How to use 

    基本上每一個按鈕的名稱就是它的功能, 要注意的地方是
    1. input text的時候, 要加上去的時候結尾要按enter, 其他操作可能會導致無法預期的後果, 我有做的一個補丁是如果選了text後, 點了canvas生出input方塊來後, 突然不想輸入了, 這時點其他功能button(畫筆, 擦子, 拉圖形的那3個), 再點canvas可以就消去input方塊

### Function description

    1. getMousePos: get mouse position in canvas
    2. canvas.addEventListener('mousedown', func)和 canvas.addEventListener('mouseup', func): 前者會在canvas裡被點擊觸發, 會先記錄當前mouse pos, 然後依照status做出相應操作, 而status會在html中由button onclick 所控制, 前者裡面包含的callback function是3~6, 而後者則是點擊結束時把event listener移除
    3. draw: set destination as current mouse positoin and stroke, pencil and eraser both use this function, while eraser have to set globalCompositeOperation to "destination-out"
    4. updateRec: 和undo有關, 內容為根據現在mouse pos畫一個rec, 但因為是mousemove listener function,他會一直被觸發, 所以每次畫之前要先回前一張圖
    5. updateCircle, updateTriangle: 和4.類似
    6. inputText: FontSize和FontStyle是在其他地方設listener去抓input tag的值, 有這兩個就可以設字型和大小, 然後接著要去canvas div下的sub div加上一的input tag, 最後加上該tag的event listener, 其function是當把字畫出來, 刪掉listener, 刪掉input tag, 最後存一張圖
    7. cursor icon: 用canvas.addEventListener的'mousemove', 根據當前status去設element.style.cursor的img
    8. colorpicker: 用event listener抓input tag的值設給ctx的fill style和stroke style
    9. brushSize, font和fontSize都和8.類似, 都是抓值存到一個變數
    10. load img: 創建Image obj, call API(createObjectURL)改它的src, 就可用drawImage畫出來了
    11. undo redo: 用一個array去存getImageData的結果, 剛開始時存一張空白的, 接下來畫一次就存一次, 並且用curFrame這個變數代表現在是第幾張, undo就putImageData回curFrame-1, 不要超出索引範圍即可, redo則相反, 除此之外如果undo後畫了新的東西, 則會把多的舊frame從array刪掉
    12. refresh(fuction 叫"cler"): 把Frame 0(一開始存的空白畫布)畫出來, 接著saveFrame
    13. blur(fuction 叫"pix"): 對每一個pixel做處理, 抓他自己以及以他為中心九宮格的所有pixel做平均, 作為自己的新顏色, 上述步驟重複5次

### Gitlab page link

    https://108062232.gitlab.io/AS_01_WebCanvas

### Others (Optional)

<style>
table th{
    width: 100%;
}
</style>
