const stateEnum = {
    pencil:1, 
    eraser:2,
    rectangle:3,
    circle:4,
    text:5,
    triangle:6,
};

var canvas = document.getElementById("myCanvas");
var ctx = canvas.getContext("2d");
var state = stateEnum.pencil;
var myImgData = [ctx.getImageData(0, 0, canvas.width, canvas.height)];
var curFrame = 0;
var drawing = false;
var curFontStyle = 'Verdana';
var curFontSize = '30';

function getMousePos(event){
    return {
        x:event.clientX - canvas.getBoundingClientRect().left,
        y:event.clientY - canvas.getBoundingClientRect().top
    }
}

// pencil
function draw(event){
    ctx.lineTo(getMousePos(event).x, getMousePos(event).y);
    ctx.stroke();
}


//rectangle
function updateRec(event){    
    // todo: delete last frame
    ctx.putImageData(myImgData[curFrame], 0, 0);

    ctx.fillRect(event.currentTarget.startPtX, event.currentTarget.startPtY,
        getMousePos(event).x - event.currentTarget.startPtX, 
        getMousePos(event).y - event.currentTarget.startPtY);
}


// circle
function dist(ax, ay, bx, by){ return Math.sqrt((ax - bx)*(ax - bx) + (ay - by)*(ay - by)); }
function updateCircle(event){  
    ctx.putImageData(myImgData[curFrame], 0, 0);

    let distance = dist(event.currentTarget.startPtX, event.currentTarget.startPtY,
        getMousePos(event).x, getMousePos(event).y);
    
    ctx.beginPath();
    // center.x, center.y, radius, start_angle, end_angle
    ctx.arc(event.currentTarget.startPtX, event.currentTarget.startPtY, distance, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.closePath();
}


// triangle
function updateTriangle(event){  
    ctx.putImageData(myImgData[curFrame], 0, 0);

    let Pt = {
        x1: (event.currentTarget.startPtX),
        y1: (event.currentTarget.startPtY),
        x2: getMousePos(event).x,
        y2: getMousePos(event).y,
    };
    let d = dist(Pt.x1, Pt.y1, Pt.x2, Pt.y2);
    
    ctx.beginPath();
    ctx.moveTo(Pt.x1 + d * Math.sqrt(3)/2, Pt.y1 + d / 2);
    ctx.lineTo(Pt.x1 - d * Math.sqrt(3)/2, Pt.y1 + d / 2);
    ctx.lineTo(Pt.x1, Pt.y1 - d);
    ctx.closePath();
    ctx.stroke();
}

// text
function inputText(){
    //ctx.font = "30px Comic Sans MS";
    ctx.font = curFontSize + 'px ' + curFontStyle;

    var div1_1 = document.getElementById('div1_1');
    div1_1.innerHTML = "<input type='txt' id='inputText'>";
    div1_1.style.top = canvas.startPtY + 'px';
    div1_1.style.left = canvas.startPtX + 'px';

    document.getElementById('inputText').addEventListener('change', function inputHandler(event){
        ctx.fillText(event.target.value, canvas.startPtX, canvas.startPtY);
        document.getElementById('inputText').removeEventListener('change', inputHandler);
        div1_1.innerHTML = "";
        saveFrame();
    });
}

// mouse control
canvas.addEventListener('mousedown', function(event){
    console.log(getMousePos(event).x, getMousePos(event).y);
    canvas.startPtX = getMousePos(event).x;
    canvas.startPtY = getMousePos(event).y;

    // destroy text input box
    document.getElementById('div1_1').innerHTML = "";

    switch(state){
        case stateEnum.pencil:
            ctx.beginPath();
            drawing = true;
            ctx.moveTo(getMousePos(event).x, getMousePos(event).y);
        
            canvas.addEventListener('mousemove', draw);
            break;
        case stateEnum.eraser:
            ctx.beginPath();
            drawing = true;
            ctx.globalCompositeOperation = "destination-out";
            ctx.moveTo(getMousePos(event).x, getMousePos(event).y);
    
            canvas.addEventListener('mousemove', draw);
            break;
        case stateEnum.rectangle:
            ctx.beginPath();
            drawing = true;

            canvas.addEventListener('mousemove', updateRec);
            break;
        case stateEnum.circle:
            ctx.beginPath();
            drawing = true;

            canvas.addEventListener('mousemove', updateCircle);
            break;
        case stateEnum.triangle:
            ctx.beginPath();
            drawing = true;

            canvas.addEventListener('mousemove', updateTriangle);
            break;
        case stateEnum.text:
            inputText();
            break;

    }
})

function saveFrame(){
    if(myImgData.length - 1 != curFrame){
        myImgData.length = curFrame + 1;
    }
    myImgData.push(ctx.getImageData(0, 0, canvas.width, canvas.height));
    curFrame = myImgData.length - 1;
}

window.addEventListener('mouseup', function() {
    
    if(drawing){
        ctx.closePath();        
        saveFrame();
        
        // turn off eventListener
        switch(state){
            case stateEnum.pencil:
                    canvas.removeEventListener('mousemove', draw);
                    break;
            case stateEnum.eraser:
                    canvas.removeEventListener('mousemove', draw);

                    // switch to default drawing setting
                    ctx.globalCompositeOperation = "source-over";
                    break;
            case stateEnum.rectangle:
                    canvas.removeEventListener('mousemove', updateRec);
                    break;
            case stateEnum.circle:
                    canvas.removeEventListener('mousemove', updateCircle);
                    break;
            case stateEnum.triangle:
                    canvas.removeEventListener('mousemove', updateTriangle);
                    break;

        }
    }
    drawing = false;
});


// cursor icon
canvas.addEventListener('mousemove', function(){
    switch(state){
        case stateEnum.pencil:
                document.getElementById('myCanvas').style.cursor = "url(img/brush.png) ,auto";
                break;
        case stateEnum.eraser:
                document.getElementById('myCanvas').style.cursor = "url(img/eraser.jpg) ,auto";
                break;
        case stateEnum.rectangle:
                document.getElementById('myCanvas').style.cursor = "url(img/rectangle.png) ,auto";
                break;
        case stateEnum.circle:
                document.getElementById('myCanvas').style.cursor = "url(img/circle.png) ,auto";
                break;
        case stateEnum.triangle:
                document.getElementById('myCanvas').style.cursor = "url(img/triangle.png) ,auto";
                break;
        case stateEnum.text:
                document.getElementById('myCanvas').style.cursor = "url(img/text.png) ,auto";
                break;
        default:
                document.getElementById('myCanvas').style.cursor = 'default';
                break;
    }
});

document.getElementById('colorPicker').addEventListener('change', function(event){
    console.log(event.target.value);
    ctx.fillStyle = event.target.value;
    ctx.strokeStyle = event.target.value;
});

document.getElementById('brushSize').addEventListener('change', function(event){
    ctx.lineWidth = event.target.value;
});

document.getElementById('font').addEventListener('change', function(event){
    curFontStyle = event.target.value;
});

document.getElementById('textSize').addEventListener('change', function(event){
    curFontSize = event.target.value;
});

// load img
document.getElementById('inputImg').addEventListener('input', function(event){
    var img = new Image();
    img.src = window.URL.createObjectURL(document.getElementById('inputImg').files[0]);

    img.onload = function(){
        ctx.drawImage(img, 0, 0);
        saveFrame();
    }
});

function undo(){
    if(curFrame > 0){
        curFrame--;
        ctx.putImageData(myImgData[curFrame], 0, 0);
    }
    console.log(curFrame);
}

function redo(){
    if(curFrame + 1 < myImgData.length){
        curFrame++;
        ctx.putImageData(myImgData[curFrame], 0, 0);
    }
    console.log(curFrame);
}

function cler(){
    ctx.putImageData(myImgData[0], 0, 0);
    saveFrame();
}

function save(){
    return document.getElementById("myCanvas").toDataURL();
}

function pix(){
    var data = myImgData[curFrame].data;
    var w = myImgData[curFrame].width;
    var h = myImgData[curFrame].height;
    var f = [-4, 4, -4-w*4, -4+w*4, -w*4, w*4, 4+w*4, 4-w*4, 0];

    for(let m = 0; m < 5; m++){
        for (let i = 0; i < data.length; i ++) {
            let tmp = 0;
            let n = 0;
    
            for(let j = 0; j < f.length; j++){
                tmp += data[i + f[j]];
            }
            tmp/=9;
            tmp = Math.floor(tmp);
    
            data[i] = tmp;
        }
    }
    
    ctx.putImageData(myImgData[curFrame], 0, 0);
    saveFrame();
}

